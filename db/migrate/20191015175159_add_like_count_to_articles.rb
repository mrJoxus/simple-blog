class AddLikeCountToArticles < ActiveRecord::Migration[5.1]
  def change
    add_column :articles, :like_count, :integer
  end
end
