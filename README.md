# Project Goals

I want to dive in a little bit deeper into (backend)development:

-   Rails basics (including best practise gem)
-   MVC pattern
-   password encrypion
    -   decrypt
    -   salt
-   session management
    -   protect from forgery
-   create simple databases with SQL (maybe Postgresql)
-   deploy on heroku
-   descriptive readme
-   BEEM CSS structure
-   mobile first development
