class SessionsController < ApplicationController
  def new
  end
  def create
    user = User.where(user_name: params[:user_name]).first
    if user && user.authenticate(password)
      session[:user_id] = user.id
      redirect_to root_url, notice: "Logged in!"
    else
      flash.now[:alert] = "username or password is invalid"
      render "new"
    end
  end
  def destroy
    session[:user_id] = nil
    redirect_to root_url, notice: "Logged out!"
  end
  private
    def password
      if params[:password] == params[:password_confirmation]
        params[:password]
      end
    end
end
