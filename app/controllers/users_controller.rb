class UsersController < ApplicationController
	def new
		@user = User.new
	end
	def create
    @user = User.new(user_params)
    if @user.save 
      @user.save
			redirect_to @user
    else
			render 'new'
    end
  end
		def show
			@user = User.find(params[:id])
		end
  private
    def user_params
      if params[:user][:password] == params[:user][:password_confirmation]
        params.require(:user).permit(:user_name, :email_address, :password, :password_confirmation)
      end
		end

end
