class CommentsController < ApplicationController
	def create
		@article = Article.find(params[:article_id])
    if current_user.id
			@comment_params = comment_params
			@comment_params[:user_id] =current_user.id
			@comment = @article.comments.create(@comment_params)
		end

      redirect_to article_path(@article)
	end

  def destroy
    if validate_user
      @article = Article.find(params[:article_id])
      @comment = Comment.find(params[:id])
      @comment.destroy

      redirect_to article_path(@article)
    end
  end

  def validate_user
		@comment = Comment.find(params[:id])
		@comment.user.id == current_user.id
	end
	private
		def comment_params
			params.require(:comment).permit(:commenter, :body)
		end
end
